﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//ask for party name
//ask for number of people in party
//add to list
//ask for party count
//count number of people in total
//print out the guest list and number of people


namespace GuestListB
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> headNames = new List<string>();
            int totalGuests = 0;
            string moreComming = "";

            do
            {
                headNames.Add(GetINfoFromConsole("What is your party name?: "));

                totalGuests += GetNumberofGuests();

               
                moreComming = GetINfoFromConsole("Do you want to add another guest? y/n: ");
            } while (moreComming.ToLower() == "y");

            foreach (string name in headNames)
            {
                Console.WriteLine(name);
                
            }
            Console.WriteLine($" Total guests: {totalGuests}");

            Console.ReadLine();

        }

        private static string GetINfoFromConsole(string message)
        {
            Console.Write(message);
            string outputName = Console.ReadLine();

            return outputName;
            
        }

        private static int GetNumberofGuests()
        {
            bool isValidNumber = false;
            int output;
            do
            {
                string partySizeText = GetINfoFromConsole("How many people in your party?: ");
                isValidNumber = int.TryParse(partySizeText, out output);

            } while (isValidNumber == false);

            return output;
        }
    }
}
